# Message board

## Introduction

This webapp will be a message board similar to reddit. Users will be able to register to the app using a username and password. Registered users will then be allowed to post messages. The API will be built using Python/Flask. The database to store usernames, passwords, and messages will be created using Elasticseach. The front end of the app will be created using React.

## Usage

Requirements to run webapp:

- Docker
- Docker-compose
- Make
- Node/Npm

To run this webapp, `git clone` this repo and run `make`. Then navigate to `http://localhost:8000` in a browser. Tested to work on Chrome.

To populate database with dummy data, run `make user` and `make post`.

## Design

```
+-----------------------+          +-------------------------+           +----------------------+
|                       |   http   |                         |   http    |                      |
|         React         |  <---->  |      Python Flask       |  <----->  |    Elasticsearch     |
|                       |          |                         |           |                      |
+-----------------------+          +-------------------------+           +----------------------+

    method: create user         endpoint: POST /users
  +-------------------+         POST http://localhost:9200/users/_doc
  | JSON body: {      |               +------------------+                +------------------+
  |  username: string |               | username: string |                | username: string |
  |  password: string |   +------>    | password: string |    +------->   | password: string |
  | }                 |               | bancount: 0      |                | bancount: int    |
  +-------------------+               | banned: False    |                | banned: bool     |
                                      | created: date    |                | created: date    |
                                      +------------------+                +------------------+

                                endpoint: POST /posts
    method: create post         POST http://localhost:9200/posts/_doc
  +-------------------+              +------------------+                 +------------------+
  | JSON body: {      |              | id: int          |                 | id: int          |
  |  username: string |              | username: string |                 | username: string |
  |  content: string  |   +------>   | content: string  |    +-------->   | content: string  |
  | }                 |              | created: date    |                 | created: date    |
  +-------------------+              +------------------+                 +------------------+

                             endpoint: GET /posts
                             POST http://localhost:9200/posts/_search     +------------------+
                                   +-----------------------+              | [                |
                                   | {                     |              | {                |
    method: view post     +-->     |   "size": 10,         |              | id: int          |
                                   |   "sort": {           |   +---->     | username: string |
                                   |     "created": "desc" |              | content: string  |
                                   |   }                   |              | created: date    |
                                   | }                     |              | } ...            |
                                   +-----------------------+              | ]                |
                                                                          +------------------+
```

## Development

### Dependencies
- Python 3.7.4
- Elasticsearch 7.3.0
- React