import React from 'react';
import axios from 'axios';
import { List } from 'semantic-ui-react'

class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: []
        }
    }
    componentDidMount() {
        axios.get('http://localhost:8000/posts')
            .then(res => {        
                this.setState({
                        data: res.data["hits"]
                });
            })
    }
    
    render() {
        const { data } = this.state
        return (
        <List divided relaxed>
            {
                data &&
                data.map((post,index) => {
                    return (
                        <List.Item>
                        <List.Content>
                          <List.Header as='a'>{ post["_source"]["username"] }</List.Header>
                          <List.Description as='a'>{ post["_source"]["content"] }</List.Description>
                        </List.Content>
                      </List.Item>
                    )
                })
            }
        </List>
        )
    }
}

export default App;