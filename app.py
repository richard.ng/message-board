import flask
from flask import Flask, render_template, request, abort
import api
import os

app = Flask(__name__, static_folder="./static/dist", template_folder="./static")

@app.route('/')
def index():
    return render_template("index.html")

@app.route('/users', methods=['POST'])
def register():
    data = request.get_json()
    if "username" not in data or "password" not in data:
        abort(400, 'JSON body must contain "username" and "password"')
    username = data["username"]
    password = data["password"]

    try:
        api.register(username, password)
    except Exception as e:
        abort(400, e)
    return data

@app.route('/posts', methods=['POST', 'GET'])
def post():
    if request.method == 'POST':
        data = request.get_json()
        if "username" not in data or "content" not in data:
            abort(400, 'JSON body must contain "username" and "content"')
        username = data["username"]
        post = data["content"]

        try:
            return api.post(username, post)
        except Exception as e:
            abort(400, e)

    try:
        return api.getall()
    except Exception as e:
        abort(400, e)

if __name__ == '__main__':
    app.secret_key = os.urandom(12)
    app.run(host='0.0.0.0', port=8000)