from elasticsearch import Elasticsearch
import elasticsearch as e
import logging

logger = logging.getLogger('MSGBOARD')
logger.setLevel(logging.DEBUG)

ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)

formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

ch.setFormatter(formatter)

logger.addHandler(ch)

es = Elasticsearch()

def schema():
    for index in 'users', 'posts':
        schema = open("{}.json".format(index)).read()
        try:
            res = es.indices.create(index=index, ignore=400, body=schema)
        except e.ElasticsearchException as error:
            logger.critical(error)
            sys.exit()
        try:
            if res['index']:
                logger.info("{} Index Successfully Created".format(index))
        except:
            logger.error("{} Index Already Exists".format(index))

schema()