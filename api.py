from datetime import datetime
from elasticsearch import Elasticsearch
from string import punctuation
import re
import elasticsearch as e
import logging

# ENV
USERSINDEX = 'users'
POSTSINDEX = 'posts'
ES_HOST = "elasticsearch"
ES_PORT = 9200

# initalize logger
logger = logging.getLogger('MSGBOARD')
logger.setLevel(logging.DEBUG)
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
logger.addHandler(ch)

# connect to ES
es = Elasticsearch([{'host': ES_HOST, 'port': ES_PORT}])

# register username, password to ES
def register(username, password):
    reg = {
        'username': username,
        'password': password,
        'bancount': 0,
        'banned' : False,
        'created': datetime.now()
    }

    # check if username already exists, if not, enter to ES
    try:
        res = es.search(index=USERSINDEX, body={"query": {"match": {"username": username}}})
        if res['hits']['hits']:
            logger.error("{} Already Exists".format(username))
            raise Exception("{} Already Exists".format(username))
        else: 
            res = es.index(index=USERSINDEX, doc_type='_doc', body=reg)
            logger.info("{} Successfully Registered".format(username))
    except e.ConnectionError as error:
        logger.critical(error)
        raise Exception("Elasticsearch not connected")

# post username, content to ES
def post(username, content):

    # check if username exists in ES
    try:
        res = es.search(index=USERSINDEX, body={"query": {"match": {"username": username}}})
    except e.ConnectionError as error:
        logger.critical(error)
        raise Exception("Elasticsearch not connected")
    try:
        if username == res['hits']['hits'][0]['_source']['username']:
            pass
    except:
        logger.error("Username: {} Not Registered".format(username))
        raise Exception("Username: {} Not Registered".format(username))

    # check if username is banned
    if res['hits']['hits'][0]['_source']['banned'] == True:
        logger.error("Username: {} is banned".format(username))
        raise Exception("Username: {} is banned".format(username))

    # get post id
    try:
        res = es.search(index=POSTSINDEX, body={"size":1000,"query": {"match_all": {}}})
    except e.ConnectionError as error:
        logger.critical(error)
        raise Exception("Elasticsearch not connected")
    num = []
    for i in range(len(res['hits']['hits'])):
        num.append(res['hits']['hits'][i]['_source']['id'])
    if not num:
        id = 1
    else:
        id = max(num)+1

    doc = {
        'id': id,
        'username': username,
        'content': content,
        'created': datetime.now()
    }

    # initialize bad words dictionary
    # split content to list of strings
    dictionary = ["bad", "horrible", "liar"]
    pun = set(punctuation)
    pun.remove("*")
    string = re.findall(r"[\w']+|[.,!?;]",content)
    
    count=0
    for words in dictionary:
        for i in range(len(string)):

            # censor bad words
            if words == string[i]:
                string[i] = "****"
                count += 1

            # if more than 3 bad words, do not post and increase ban counter
            if count >= 3:

                # get user _id and current bancount
                try:
                    res = es.search(index=USERSINDEX, body={"query": {"match": {"username": username}}})
                except e.ConnectionError as error:
                    logger.critical(error)
                    raise Exception("Elasticsearch not connected")
                userid = res['hits']['hits'][0]['_id']
                bancount = res['hits']['hits'][0]['_source']['bancount']
                doc = {
                    "doc": {
                    'bancount': bancount+1
                    }
                }

                # update user with bancount +1
                try:
                    es.update(index=USERSINDEX, doc_type='_doc', body=doc, id=userid)
                    es.indices.refresh(index=USERSINDEX)
                except e.ConnectionError as error:
                    logger.critical(error)
                    raise Exception("Elasticsearch not connected")

                # get updated bancount
                try:
                    res = es.search(index=USERSINDEX, body={"query": {"match": {"username": username}}})
                except e.ConnectionError as error:
                    logger.critical(error)
                    raise Exception("Elasticsearch not connected")
                bancount = res['hits']['hits'][0]['_source']['bancount']

                # if more than 3 rejected messages, ban user
                if bancount >= 3:

                    # get user
                    try:
                        res = es.search(index=POSTSINDEX, body={"query": {"match": {'username': username}}})
                    except e.ConnectionError as error:
                        logger.critical(error)
                        raise Exception("Elasticsearch not connected")

                    # remove user posts
                    for i in range(len(res['hits']['hits'])):
                        docid = res['hits']['hits'][i]['_id']
                        try:
                            es.delete(index=POSTSINDEX, doc_type='_doc', id=docid)
                        except e.ConnectionError as error:
                            logger.critical(error)
                            raise Exception("Elasticsearch not connected")

                    # add banned flag to user
                    try:
                        es.update(index=USERSINDEX, doc_type='_doc', id=userid, body={"doc":{"banned":True}})
                    except e.ConnectionError as error:
                        logger.critical(error)
                        raise Exception("Elasticsearch not connected")

                    logger.error("Too many inappropriate messages, you have been banned")        
                    raise Exception("Too many inappropriate messages, you have been banned")

                logger.error("Too many forbidden words, message not posted")
                raise Exception("Too many forbidden words, message not posted")

    # join list of strings after censoring            
    string = ''.join(w if set(w) <= pun else ' '+w for w in string).strip()

    doc = {
        'id': id,
        'username': username,
        'content': string,
        'created': datetime.now()
    }

    # post to ES
    try:
        es.index(index=POSTSINDEX, doc_type='_doc', body=doc)
    except e.ConnectionError as error:
        logger.critical(error)
        raise Exception("Elasticsearch not connected")

    logger.info(doc)
    return string

def getall():

    # get all posts
    try:
        res = es.search(index=POSTSINDEX, body={"size":1000,"sort":{"created":"desc"},"query": {"match_all": {}}})
    except e.ConnectionError as error:
        logger.critical(error)
        raise Exception("Elasticsearch not connected")

    for i in range(len(res['hits']['hits'])):
        logger.info(res['hits']['hits'][i]['_source'])
    return res['hits']

def get(id):

    # get post by id
    try:
        res = es.search(index=POSTSINDEX, body={"query": {"match": {'id': id}}})
    except e.ConnectionError as error:
        logger.critical(error)
        raise Exception("Elasticsearch not connected")

    try:
        logger.info(res['hits']['hits'][0]['_source'])
        return res['hits']
    except:
        logger.error("Post id does not exist")