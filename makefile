flaskapp: es index flask

es:
	docker-compose up -d elasticsearch
	@echo "waiting for ES"
	@sleep 15

index:
	@curl -s -H "content-type: application/json" -d "@users.json" -XPUT http://localhost:9200/users
	@curl -s -H "content-type: application/json" -d "@posts.json" -XPUT http://localhost:9200/posts

flask:
	cd static && npm install && npm run build
	docker build -t app:test .
	docker-compose up -d flask

user:
	@curl -H "content-type: application/json" -XPOST -d '{"username":"user1","password":"pass1"}' localhost:8000/users

post:
	@curl -H "content-type: application/json" -XPOST -d '{"username":"user1","content":"good post"}' localhost:8000/posts

clean:
	docker-compose down -v