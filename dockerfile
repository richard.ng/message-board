# FROM node:12.7.0-alpine

# ADD ./ /app
# WORKDIR /app/static/
# RUN npm install && npm run build

# Use an official python3 image 
FROM python:3.7.4-alpine3.9
ADD ./ /app
# COPY --from=0 /app /app

# set the working directory of container to app 
WORKDIR /app

# install the needed packages
RUN apk update && apk upgrade && pip install -r requirements.txt

# make port 8000 available to the world outside of the container 
EXPOSE 8000 

# commands to run the file when we launch container
CMD ["python", "app.py"]
